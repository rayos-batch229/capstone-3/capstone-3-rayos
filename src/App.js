import {useState} from 'react';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import {UserProvider} from './userContext';
import './App.css';
import Landing from './pages/Landing';
import Register from './pages/Register';
import Login from './pages/Login';
import Home from './pages/Home';
import ProductView from './components/ProductView';
import Logout from './pages/Logout';

function App() {

  const [user, setUser] = useState({

    token: localStorage.getItem('token'),
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  return(
    <UserProvider value={{user, setUser, unsetUser}}>
      <>
        <Router>
            <Routes>
                <Route exact path="/" element={<Landing/>}/>
                <Route exact path="/register" element={<Register/>}/>
                <Route exact path="/login" element={<Login/>}/>
                <Route exact path="/home" element={<Home/>}/>
                <Route exact path="/product/:productId" element={<ProductView/>}/>
                <Route exact path="/logout" element={<Logout/>}/>
            </Routes>
        </Router>
      </>
    </UserProvider>   
  );
}

export default App;
