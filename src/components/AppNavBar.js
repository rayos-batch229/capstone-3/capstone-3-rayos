import {useContext} from 'react';
import {Nav, Navbar, Image} from 'react-bootstrap';
import {NavLink, Outlet, Link} from 'react-router-dom';
import userContext from '../userContext';
import BrandName from '../images/download.png';

export default function AppNavBar() {

    const {user} = useContext(userContext);

    return (
        <>
        <Navbar expand="md" className="solid-background p-3 font-pt" sticky="top">
            <Nav.Item> 
                        <Nav.Link as={NavLink} exact to="/home" id='item-link'>Angel's Computer</Nav.Link>
                    </Nav.Item>
            <Navbar.Brand as={Link} to={'/home'}><Image src={BrandName} className='w-25 span mx-3'></Image></Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="justify-content-end">
                    <Nav.Item> 
                        <Nav.Link as={NavLink} exact to="/home" id='item-link'>Items</Nav.Link>
                    </Nav.Item>
                    {
                        (user.token !== null) ? 
                        <>
                            <Nav.Item> 
                            <Nav.Link as={NavLink} exact to="/" id='item-link'>Basket</Nav.Link> 
                            </Nav.Item>
                            <Nav.Item> 
                            <Nav.Link as={NavLink} exact to="/logout" id='item-link'>Logout</Nav.Link> 
                            </Nav.Item>
                        </> : 
                        <>
                            <Nav.Item>
                            <Nav.Link as={NavLink} exact to="/login" id='item-link'>Login</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                            <Nav.Link as={NavLink} exact to="/register" id='item-link'>Register</Nav.Link>
                            </Nav.Item>
                        </>
                    }
                    
                </Nav>
            </Navbar.Collapse> 
        </Navbar>
        <Outlet/>
        </>
    )
}
