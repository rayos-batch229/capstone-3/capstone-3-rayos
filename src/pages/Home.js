import {useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Image, Button, Modal, Form, Nav} from 'react-bootstrap';
import {Navigate,useNavigate,NavLink} from 'react-router-dom';
import AppNavBar from '../components/AppNavBar';
import userContext from '../userContext'; 
import ProductList from '../components/ProductList';
// import cover from '../images/official-banner.png';

export default function Home() {

	const {user} = useContext(userContext);

	const navigate = useNavigate();

	const [productName, setProductName] = useState('');
	const [productBrand, setProductBrand] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const	[isActive, setIsActive] = useState(false);
	const [products, setProducts] = useState([]);
	const [activeProducts, setActiveProducts] = useState([]);
	const [show, setShow] = useState(false);

	// const [name, setName] = useState('');
	// const [description, setDescription] = useState('');
	// const [price, setPrice] = useState('');
	// const [isActive, setIsActive] = useState(false)

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'  
			}
		})
    .then(res => res.json())
    .then(data => {

      setProducts(data.map(product => {
          return (
						<>
							<ProductList key={product._id} productProps={product}/>
						</>
					)
      }))
    })
	}, [])

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/active`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		})
    .then(res => res.json())
    .then(data => {

      setActiveProducts(data.map(activeProduct => {
        return (
					<>
						<ProductList key={activeProduct._id} productProps={activeProduct}/>
					</>
				)
      }))
    })
	}, [])

	useEffect(() => {

		if(productName !== '' && productBrand !== '' && description !== '' && price !== '') {
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
		
	}, [productName, productBrand, description, price]);

	function createProduct(e) {

		e.preventDefault();

		fetch('${process.env.REACT_APP_API_URL}/products/add', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				productName: productName,
				productBrand: productBrand,
				description: description,
				price: price

			})
		})
		.then(res => res.json())
		.then(data => {

			if(data) {

				navigate('/home');
			}
		})

		setProductName('')
		setProductBrand('')
		setDescription('');
		setPrice('');

	}

	return (
		<>
			<AppNavBar/>
			{
				(user.isAdmin === true) ?
					<Container className='text-center'>
						<h1 className='text-center mt-5'>Products List</h1>
						<Button onClick={handleShow} className="submitBtn">Update</Button>
						<Row className='px-5'>
							<Col className="px-5 py-3 text-center">
								{products}
							</Col>
						</Row>
					</Container>
				: 
				<Container>
						<Row>
							<Col className="p-5 text-center">
								{/*<Image src={cover}></Image>*/}
							</Col>
						</Row>
						<h1 className='text-center'>Computer Peripherals</h1>
						<Row className='px-5'>
							<Col className="px-5 py-3 text-center">
								{activeProducts}
							</Col>
						</Row>
					</Container>
			}
			
		  <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false}>
	      <Form onSubmit={e => createProduct(e)}>
	        <Modal.Header closeButton>
	          <Modal.Title>Create Another Product</Modal.Title>
	        </Modal.Header>
	        <Modal.Body>

			      <Form.Group className="mb-3" controlId="name">
			        <Form.Label>Product Name</Form.Label>
			        <Form.Control type="text" placeholder="enter product name" 
			        							value={productName} 
			        							onChange={e => setProductName(e.target.value)} required/>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="name">
			        <Form.Label>Brand Name</Form.Label>
			        <Form.Control type="text" placeholder="enter product brand" 
			        							value={productBrand} 
			        							onChange={e => setProductBrand(e.target.value)} required/>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="description">
			        <Form.Label>Product Description</Form.Label>
			        <Form.Control type="textarea" placeholder="enter product description here" 
			        							value={description} 
			        							onChange={e => setDescription(e.target.value)}required/>
			      </Form.Group>

				    <Form.Group className="mb-3" controlId="price">
			        <Form.Label>Product Price</Form.Label>
			        <Form.Control type="number" placeholder="enter price" 
			        							value={price} 
			        							onChange={e => setPrice(e.target.value)} required/>
			      </Form.Group>
	        </Modal.Body>
	        <Modal.Footer>
	          <Button className="cancelBtn" onClick={handleClose}>Cancel</Button>
	          {
							isActive ? 
							<Button type="submit" className="confirmBtn" onClick={handleClose}>Add</Button> 
							: <Button type="submit" className="confirmBtn" disabled>Add</Button>
						}
	        </Modal.Footer>
	      </Form>
	    </Modal>
		</>
	);
}; 
