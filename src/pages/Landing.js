import {Container, Row, Col, Image, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Home() {
	return (
		<>
			<div className='black-background min-vh-100'>
				<Container fluid className='font-roboto pt-5'>
					<Row className="justify-content-center">
						<Col>
							<Button as={Link} to={'/home'} className='mx-auto d-block w-50'>Welcome to Angel's Computer Store</Button>
						</Col>
					</Row>
				</Container>
			</div>
		</>
	);
}; 
