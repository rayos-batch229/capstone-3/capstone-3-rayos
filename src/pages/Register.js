import {useState, useEffect, useContext} from 'react';
import {Container, Form, Button, Nav} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import AppNavBar from '../components/AppNavBar';
import swal from 'sweetalert2';
import userContext from '../userContext'; 

export default function Register() {

  const navigate = useNavigate();

  const {user} = useContext(userContext);
  
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    
    if((firstName !== '' && lastName !== '' && email !== '' && mobileNo !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
      setIsActive(true)
    }else{
      setIsActive(false)
    }
  },[firstName,email,lastName,mobileNo,password1,password2])


  function registerUser(e) {

    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL }/users/checkEmail`,{
      method: 'Post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email

      })
    },[])

    .then(result => result.json())
    .then(data => {
        if(data !== true){
          fetch(`${process.env.REACT_APP_API_URL }/users/register`,{
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              mobileNo: mobileNo,
              password: password1
            })
          },[])

          swal.fire({
                  title: "Registration Successfull",
                  icon: "success",
                  text: "Welcome Boi!"
            })
            navigate("/login")    
                      
          setFirstName('')
          setLastName('')
          setEmail('');
          setMobileNo('');
          setPassword1('');
          setPassword2('');
          

          
        }else{
          swal.fire({
                  title: "Email Already Exists Boi!!!",
                  icon: "warning",
                  text: "Please Provide A Different Email!"
            })
        }
    })
  }

  return (

    (user.token === null) ?
    <>
      <AppNavBar/>
      <div className='black-background min-vh-100'>
        <Container className='container-box px-2 py-2 mt-4'>
          <h2 className='text-center'>Register Now!</h2>
          <Form onSubmit={e => registerUser(e)}>

            <Form.Group className="mb-3" controlId="firstName">
                  <Form.Label>First Name</Form.Label>
                  <Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
              </Form.Group>

              <Form.Group className="mb-3" controlId="lastName">
                  <Form.Label>Last Name</Form.Label>
                  <Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)}required/>
              </Form.Group>

              <Form.Group className="mb-3" controlId="userEmail">
                  <Form.Label>Email Address</Form.Label>
                  <Form.Control type="email" placeholder="Enter mobile number" value={email} onChange={e => setEmail(e.target.value)}required/>
              </Form.Group>

            <Form.Group className="mb-3" controlId="mobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control type="text" placeholder="Enter last name"  value={mobileNo} onChange={event => setMobileNo(event.target.value)} required/>
            </Form.Group>           

            <Form.Group className="mb-3" controlId="Password1">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" value={password1} onChange={event => setPassword1(event.target.value)} required/>
            </Form.Group>

            <Form.Group className="mb-3" controlId="Password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={event => setPassword2(event.target.value)} required />
            </Form.Group>
       <div className='text-center'>
            {
                isActive ? <Button type="submit" className="submitBtn">Register</Button> 
                :          <Button type="submit" className="submitBtn" disabled>Register</Button>
              }
              </div>
            </Form>
        </Container>
      </div>
    </>
    : <Navigate to="/home"/>
  );  
};
