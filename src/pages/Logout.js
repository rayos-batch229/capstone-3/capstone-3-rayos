import {useContext, useEffect} from 'react';
import {Navigate} from 'react-router-dom';
import userContext from '../userContext';

export default function Logout() {

	const {unsetUser, setUser} = useContext(userContext);

	unsetUser();

	useEffect(() => {
		setUser({
		token: localStorage.getItem(''),
		id: null
		})
	})

	return(
		<Navigate to="/login"/>
	)
}
