import {useState, useEffect, useContext} from 'react';
import {Container, Form, Button, Nav} from 'react-bootstrap';
import {Navigate, NavLink} from 'react-router-dom';
import AppNavBar from '../components/AppNavBar';
import swal from 'sweetalert2';
import userContext from '../userContext'; 

export default function Login() {
  
  const {user, setUser} = useContext(userContext);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const[isActive, setIsActive] = useState(false);

  console.log(email);
  console.log(password)

  
  useEffect(() => {
    
    if(email !== '' && password !== ''){
      setIsActive(true)
    }else{
      setIsActive(false)
    }
  }, [email, password])




function userLogin(event) {

    
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL }/users/login`,{
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(result => result.json())
    .then(data =>{
      console.log(data)

    
      if(typeof data.access !== "undefined"){
        localStorage.setItem('token', data.access)
        
      retrieveUserDetails(data.access);
        console.log("sdiasdisa" + data.access)
            swal.fire({
              title: "Login Successfull!",
              icon: "success",
              text: "Welcome Boi!",
              color: '#FFFFFF',
              background: '#0A0A0A',
              showConfirmButton: true
            })
        }else {
          console.log(data)
          swal.fire({
            title: "Authentication Failed",
            icon: "error",
            text: "Check your Login Details and Try Again BOI !!!",
            color: '#FFFFFF',
            background: '#0A0A0A',
            showConfirmButton: true,

          }).then(function() {
            window.location = "/login";
        });
        }
    })


    setEmail('');
    setPassword('');
    
  }


  const retrieveUserDetails = (token) =>{
    

      fetch(`${process.env.REACT_APP_API_URL }/users/details`, {
        method: 'Post',
        headers:{
          Authorization: `access ${token}`
        }
      })
      .then(res => res.json())
      .then(data =>{
        console.log(data);
        localStorage.setItem('id', data._id)
        localStorage.setItem('isAdmin', data.isAdmin)
        
        setUser({
          token: localStorage.getItem('token'),
          id: data._id,
          isAdmin: data.isAdmin
        })
        
      })


  }
  console.log(user)

  return(

    (user.token != null)?
    <Navigate to="/"/>
    
    :
<div className='black-background min-vh-100'>
    <Container  className="container-box px-2 py-2 mt-4">
        <Form onSubmit={event=> userLogin(event)} >
          <h2 className="text-center">Login</h2>
          <Form.Group className="mb-3" controlid="userEmail">
            <Form.Label>Email</Form.Label>
              <Form.Control 
              type="email" 
              placeholder="Enter email"  
              value={email} 
              onChange={event => setEmail(event.target.value)} 
              required
              />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group className="mb-3" controlid="Password">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" value={password} onChange={event => setPassword(event.target.value)} required/>
          </Form.Group>

        <div className="d-inline-block ">
           {
            isActive? 
            <Button variant="success" type="submit" controlid="submitBtn" >Login</Button> 
            : 
            <Button variant="success" type="submit" controlid="submitBtn" disabled>Login</Button> 
           }
           
        </div>
      <Nav.Link  className="nav  d-inline-block float-right " as={NavLink} exact to="/register">Don't have an account?</Nav.Link>
        </Form>
    </Container>
    </div>   
    )
}
